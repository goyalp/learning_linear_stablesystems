close all
load ./../Results/Cylinder/ImposeStability/dominant_eigen_vectors_stability_gua.mat

nx = 199; ny= 449;

%% Plot DMD modes
for i=1:2:11
    f1 = plotCylinder(reshape(real(vecs(:,i)),nx,ny),nx,ny);
    saveas(f1,sprintf('Cylinder_eigenvecs_%d.png',i))    
    f2 = plotCylinder(reshape(imag(vecs(:,i)),nx,ny),nx,ny);
    saveas(f2,sprintf('Cylinder_eigenvecs_%d.png',i+1))
end

close all
load ./../Results/Cylinder/ImposeNoStability/dominant_eigen_vectors_no_stability_gua.mat

nx = 199; ny= 449;

%% Plot DMD modes
for i=1:2:11
    f1 = plotCylinder(reshape(real(vecs(:,i)),nx,ny),nx,ny);
    saveas(f1,sprintf('Cylinder_eigenvecs_no_stabilitygua_%d.png',i))    
    f2 = plotCylinder(reshape(imag(vecs(:,i)),nx,ny),nx,ny);
    saveas(f2,sprintf('Cylinder_eigenvecs_no_stabilitygua_%d.png',i+1))
end