# Inference of Continuous Linear Systems from Data with Guaranteed Stability

This repository contains the Python implementation using the PyTorch framework to learn linear stable systems. We have also blended the fourth-order Runge-Kutta scheme; thus, the method does not require the computation of derivative information to learn dynamical models. Hence, it holds a key advantage when data are corrupted and sparsely sampled. 

The important steps of the methodology are:

1. Collect measurement data  
2. Utilize parameterization for linear dynamical systems
3. Set-up an optimization problem by incorportating the fourth-order Runge-Kutta scheme
4. Solve the optimization problem using gradient-decent 
 	
we utilize automatic differentiation implemented in PyTorch. 


## Contains
* There are three examples, `Examples_Fluid`, `Examples_Superposedflow`, and `Examples_Burgers` considered in the paper. The results generated from these examples will be saved in the Result folder. All three examples can be run by using `run_examples.sh` that contains all configurations for all three examples.
* For generating eigenfunctions for the cylinder-wake example, please run `plotfigures_cylinderwake.m` file from the folder `plotting_cylinder_eigenfuncs`. It requires `MATLAB`. 

## Dependencies
For reproducibility, we have stored all dependencies and their versions in `environment.yml`. An virtual enviornment, namely `linearstable` can be created using `conda` by using `conda env create -f environment.yml`. For running, matlab files, it requires MATLAB, and it has been tested on MATLAB2020b. 

## Licenese
See the [LICENSE](LICENSE) file for license rights and limitations (MIT).



## References
[1]. P. Goyal, I. Pontes Duff and P. Benner, [nference of Continuous Linear Systems from Data with Guaranteed Stability](https://arxiv.org/abs/2301.10060), arXiv:2301.10060, 2023.
<details><summary>BibTeX</summary><pre>
@TechReport{morGoyPB23,
  author =       {Goyal, P., Pontes Duff, I., and Benner, P.},
  title =        {Inference of Continuous Linear Systems from Data with Guaranteed Stability},
  institution =  {arXiv},
  year =         2023,
  type =         {e-print},
  number =       {2301.10060},
  url =          {https://arxiv.org/abs/2301.10060},
  note =         {cs.LG}
}
</pre></details>

## Contact
For any further query, kindly contact [Pawan Goyal](mailto:goyalp@mpi-magdeburg.mpg.de). 





