import argparse
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
import warnings
from dataclasses import dataclass

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib.patches import Rectangle
from scipy.io import loadmat, savemat
from torch.utils.data import DataLoader

import modules
from utils import reproducibility_seed

reproducibility_seed(seed=42)  # setting seed for reproducibility

font = {"size": 20}

matplotlib.rc("font", **font)
warnings.filterwarnings("ignore")
plt.rcParams["text.usetex"] = True


@dataclass
class parameters:
    bs: int = 8
    num_epochs: int = 12000
    weight_decay: float = 0.0
    sys_order: int = 3
    time_step: float = 0.05
    stability_guarantee: bool = True
    plot_eigenvalue_comparison: bool = True


Params = parameters()
parser = argparse.ArgumentParser()
parser.add_argument(
    "--imposestability", action="store_true", help="Enforcing stability"
)
parser.add_argument(
    "--plot_eigenvalue_comparison",
    action="store_true",
    help="Plotting eigen-value comparisons",
)
parser.add_argument("--epochs", type=int, default=2000, help="Number of epochs")


args = parser.parse_args()
Params.stability_guarantee = args.imposestability
Params.plot_eigenvalue_comparison = args.plot_eigenvalue_comparison
Params.num_epochs = args.epochs
print(Params)

if Params.stability_guarantee:
    Params.path = "./Results/Superposedflow/ImposeStability/"
else:
    Params.path = "./Results/Superposedflow/ImposeNoStability/"

if not os.path.exists(Params.path):
    os.makedirs(Params.path)


data = loadmat("./data/superposed_quadgyre.mat")
X = data["total_data"][:, :]
U, S, V = (
    data["Us"],
    data["Ds"].reshape(
        -1,
    ),
    data["Vs"],
)
plt.semilogy(S)
plt.xlim([0, 10])
energy_captured = sum(S[: Params.sys_order]) / sum(S) * 100
print(f"Total energy captured by {Params.sys_order} modes: {energy_captured:.4e}!")


r = Params.sys_order
Xr = U[:, :r].T @ X
fac_x = np.max(np.abs(Xr), axis=1)
D = np.diag(np.ones_like(fac_x)) / max(fac_x)
D = np.diag(np.ones_like(fac_x))
Xr_nor = D @ Xr
X_train = np.expand_dims(Xr_nor.T, axis=0)
X_true = X_train
ts = np.arange(0, X_train.shape[1] * Params.time_step, Params.time_step)
train_dset = list(zip(torch.tensor(X_train[:, :, :]).double()))
train_dl = DataLoader(train_dset, batch_size=Params.bs, shuffle=True)

dataloaders = {"train": train_dl}


if Params.stability_guarantee:
    modelDyn = modules.ModelHypothesisStable(
        sys_order=Params.sys_order, B_term=False
    ).double()
else:
    modelDyn = modules.ModelHypothesis(
        sys_order=Params.sys_order, B_term=False
    ).double()

models = {"modelDyn": modelDyn}

opt_func = torch.optim.Adam(
    [
        {
            "params": models["modelDyn"].parameters(),
            "weight_decay": Params.weight_decay,
        },
    ]
)

scheduler = torch.optim.lr_scheduler.CyclicLR(
    opt_func,
    step_size_up=2000,
    mode="triangular2",
    cycle_momentum=False,
    base_lr=1e-6,
    max_lr=5e-2,
)


models, loss_track, lr_track = modules.training(
    models, dataloaders, opt_func, Params, scheduler=scheduler
)


A_learn = models["modelDyn"].A.detach().numpy()
B_learn = models["modelDyn"].B.detach().numpy()

A_learn = D @ A_learn @ np.linalg.inv(D)
[eigs, vec] = np.linalg.eig(A_learn)
print(eigs)
Phi = (
    np.concatenate([X])
    @ V[: Params.sys_order].T
    @ np.diag(1 / S[: Params.sys_order])
    @ vec
)

if Params.stability_guarantee:
    FILE_PATH = Params.path + "dominant_eigen_vectors_stability_gua.mat"
else:
    FILE_PATH = Params.path + "dominant_eigen_vectors_no_stability_gua.mat"

savemat(FILE_PATH, {"vecs": Phi, "eigs": eigs})


font = {"size": 20}

matplotlib.rc("font", **font)
LABELS = [r"\texttt{LSI}", r"\texttt{sLSI}"]


prop_cycle = plt.rcParams["axes.prop_cycle"]
colors = prop_cycle.by_key()["color"]

if Params.plot_eigenvalue_comparison:
    eigs_no_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeNoStability/"
        + "dominant_eigen_vectors_no_stability_gua.mat"
    )["eigs"]
    eigs_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeStability/"
        + "dominant_eigen_vectors_stability_gua.mat"
    )["eigs"]
    fig, ax = plt.subplots(1, 2, figsize=(10, 4))
    ax[0].scatter(
        np.real(eigs_no_stability_gau),
        np.imag(eigs_no_stability_gau),
        color=colors[0],
        linewidths=2,
        marker="o",
        s=100,
        facecolor="none",
        label=LABELS[0],
    )
    ax[0].scatter(
        np.real(eigs_stability_gau),
        np.imag(eigs_stability_gau),
        color=colors[1],
        linewidths=2,
        marker="d",
        s=100,
        facecolor="none",
        label=LABELS[1],
    )
    ax[0].set(xlabel="real part", ylabel="imag part")
    ax[0].add_patch(Rectangle((-0.00005, -11), 0.0001, 22, color=colors[7], alpha=0.5))

    ax[1].scatter(
        np.real(eigs_no_stability_gau),
        np.imag(eigs_no_stability_gau),
        color=colors[0],
        linewidths=2,
        marker="o",
        s=100,
        facecolor="none",
        label=LABELS[0],
    )
    ax[1].scatter(
        np.real(eigs_stability_gau),
        np.imag(eigs_stability_gau),
        color=colors[1],
        linewidths=2,
        marker="d",
        s=100,
        facecolor="none",
        label=LABELS[1],
    )
    ax[1].set(xlabel="real part", ylabel="imag part")
    ax[1].set_facecolor(colors[7])
    ax[1].set_xticks([-1e-8, 0, 1e-8])
    ax[0].legend()
    plt.xlim([-1e-8, 1e-8])
    # plt.ylim([-80, 80])
    plt.tight_layout()

    fig.savefig(Params.path + "../" + "eigenvalue_comparison.pdf")
    fig.savefig(Params.path + "../" + "eigenvalue_comparison.png", dpi=300)


# Plotting dominant eigen-functions
font = {"size": 12}

matplotlib.rc("font", **font)

xlist = np.linspace(-1.5, 1.5, 200)
ylist = np.linspace(-1.5, 1.5, 200)
X, Y = np.meshgrid(xlist, ylist)

fig, ax = plt.subplots(2, 4, figsize=(10, 5))

for k in range(2):
    phi1 = Phi[:40000, k].reshape(200, 200)
    phi1_real = np.real(phi1)
    phi2 = Phi[40000:, k].reshape(200, 200)
    phi2_real = np.real(phi2)

    ax[0][2 * k].imshow(phi1_real)
    ax[0][2 * k].set(xlabel="$x$", ylabel="$y$", title="eigenfunc for $u$")
    ax[1][2 * k].imshow(phi2_real)
    ax[1][2 * k].set(xlabel="$x$", ylabel="$y$", title="eigenfunc for $v$")

    phi1 = Phi[:40000, k].reshape(200, 200)
    phi1_imag = np.imag(phi1)
    phi2 = Phi[40000:, k].reshape(200, 200)
    phi2_imag = np.imag(phi2)

    ax[0][2 * k + 1].imshow(phi1_imag)
    ax[0][2 * k + 1].set(xlabel="$x$", ylabel="$y$", title="eigenfunc for $u$")
    ax[1][2 * k + 1].imshow(phi2_imag)
    ax[1][2 * k + 1].set(xlabel="$x$", ylabel="$y$", title="eigenfunc for $v$")

plt.tight_layout()

fig.savefig(Params.path + "eigenfunc.pdf")
fig.savefig(Params.path + "eigenfunc.png", dpi=300)
