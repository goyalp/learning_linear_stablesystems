import argparse
import os, sys

os.environ['KMP_DUPLICATE_LIB_OK']='True'

import warnings
from dataclasses import dataclass

import torch



import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Rectangle
from scipy.io import loadmat, savemat
from scipy.integrate import solve_ivp
from torch.utils.data import DataLoader



import modules
from utils import reproducibility_seed

reproducibility_seed(seed=42)  # setting seed for reproducibility

font = {"size": 18}

matplotlib.rc("font", **font)
warnings.filterwarnings("ignore")
plt.rcParams["text.usetex"] = True


from matplotlib import cm
from matplotlib.ticker import LinearLocator
from mpl_toolkits.mplot3d import Axes3D


@dataclass
class parameters:
    bs: int = 14
    num_epochs: int = 4000
    weight_decay: float = 1e-8
    stability_guarantee: bool = False
    plot_eigenvalue_comparison: bool = True
    sys_order: int = None


Params = parameters()
parser = argparse.ArgumentParser()
parser.add_argument(
    "--imposestability", action="store_true", help="Enforcing stability"
)
parser.add_argument(
    "--plot_eigenvalue_comparison",
    action="store_true",
    help="Plotting eigen-value comparisons",
)
parser.add_argument("--epochs", type=int, default=2000, help="Number of epochs")

args = parser.parse_args()
Params.stability_guarantee = args.imposestability
Params.plot_eigenvalue_comparison = args.plot_eigenvalue_comparison
Params.num_epochs = args.epochs

print(Params, Params.plot_eigenvalue_comparison)


if Params.stability_guarantee:
    Params.path = "./Results/Burgers/ImposeStability/"
else:
    Params.path = "./Results/Burgers/ImposeNoStability/"

if not os.path.exists(Params.path):
    os.makedirs(Params.path)


data = loadmat("./data/Burger_data_init_condions.mat")

X = data["xo"].T
X_all = data["X_data"].transpose(0, 2, 1)

idxs = list(np.arange(0, 17))
testing_idxs = list([4, 8, 12])
train_idxs = list(set(idxs) - set(testing_idxs))

X_testing = X_all[testing_idxs]
X_training = X_all[train_idxs]

t = data["t"].T
print(f"Training trajectories: {X_training.shape[0]}")
print(f"Training trajectories: {X_testing.shape[0]}")


# Preparing data for learning...
X_training_true = X_training
t_true = t

p = 2  # in training data, data are sampled at time-interval (p * dt)
X_training_sampled = X_training[..., ::p]
t = t[::p]

# Retain only the first k snapshots/inputs for training the ROM.
t_train = t  # Temporal domain for training snapshots.
X = X_training_sampled  # Training snapshots.

num_inits = X.shape[0]
print(f"shape of X for training:\t{X.shape}")
print(f"Training samples: {num_inits}")


# Compute SVD in order to prepare low-dimensional data
temp_X = X[0]

for i in range(1, X.shape[0]):
    temp_X = np.hstack((temp_X, X[i]))

[U, S, V] = np.linalg.svd(temp_X)


tol = 1e-3

r = 1
while r < len(S) + 1:
    if 1 - sum(S[:r]) / sum(S) < tol:
        break
    r += 1

Params.sys_order = r
print(f"Order of reduced model: {r}")
print(f"Energy captured by the snapshots: {100*sum(S[:r])/sum(S)}%")


# preparing reduced data, approximating derivative information, and dataloader for training
temp_Xr = U[:, :r].T @ temp_X  # reduced data

fac_x = np.max(np.abs(temp_Xr), axis=1)

temp_Xr = temp_Xr

Xr = np.zeros((num_inits, r, temp_Xr.shape[-1] // num_inits))
# dXr = np.zeros((num_inits, r, temp_Xr.shape[-1]//num_inits))

for i in range(0, num_inits):
    temp = int(temp_Xr.shape[-1] // num_inits)
    temp_x = temp_Xr[:, i * temp : (i + 1) * temp]
    Xr[i] = temp_x


# Define dataloaders
t1 = (torch.arange(0, Xr.shape[-1]) * (t_train[1] - t_train[0])).reshape(-1, 1)
dt = (t1[1] - t1[0]).item()
Params.time_step = dt

train_dset = list(
    zip(
        torch.tensor(Xr).permute((0, 2, 1)).double(),
    )
)
train_dl = torch.utils.data.DataLoader(train_dset, batch_size=Params.bs, shuffle=True)
dataloaders = {"train": train_dl}


if Params.stability_guarantee:
    modelDyn = modules.ModelHypothesisStable(
        sys_order=Params.sys_order, B_term=False
    ).double()
else:
    modelDyn = modules.ModelHypothesis(
        sys_order=Params.sys_order, B_term=False
    ).double()

models = {"modelDyn": modelDyn}

opt_func = torch.optim.Adam(
    [
        {
            "params": models["modelDyn"].parameters(),
            "weight_decay": Params.weight_decay,
        },
    ]
)

scheduler = torch.optim.lr_scheduler.CyclicLR(
    opt_func,
    step_size_up=2000,
    mode="triangular2",
    cycle_momentum=False,
    base_lr=1e-6,
    max_lr=1e-2,
)

models, loss_track, lr_track = modules.training(
    models, dataloaders, opt_func, Params, scheduler=scheduler
)

A_learn = models["modelDyn"].A.detach().numpy()
B_learn = models["modelDyn"].B.detach().numpy()

[eigs, vec] = np.linalg.eig(A_learn)
Phi = (
    np.concatenate([temp_X])
    @ V[: Params.sys_order].T
    @ np.diag(1 / S[: Params.sys_order])
    @ vec
)

if Params.stability_guarantee:
    FILE_PATH = Params.path + "dominant_eigen_vectors_stability_gua.mat"
else:
    FILE_PATH = Params.path + "dominant_eigen_vectors_no_stability_gua.mat"

savemat(FILE_PATH, {"vecs": Phi, "eigs": eigs})


font = {"size": 20}

matplotlib.rc("font", **font)
LABELS = [r"\texttt{LSI}", r"\texttt{sLSI}"]


prop_cycle = plt.rcParams["axes.prop_cycle"]
colors = prop_cycle.by_key()["color"]

if Params.plot_eigenvalue_comparison:
    eigs_no_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeNoStability/"
        + "dominant_eigen_vectors_no_stability_gua.mat"
    )["eigs"]
    eigs_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeStability/"
        + "dominant_eigen_vectors_stability_gua.mat"
    )["eigs"]
    fig, ax = plt.subplots(1, 2, figsize=(10, 4))
    ax[0].scatter(
        np.real(eigs_no_stability_gau),
        np.imag(eigs_no_stability_gau),
        color=colors[0],
        linewidths=2,
        marker="o",
        s=100,
        facecolor="none",
        label=LABELS[0],
    )
    ax[0].scatter(
        np.real(eigs_stability_gau),
        np.imag(eigs_stability_gau),
        color=colors[1],
        linewidths=2,
        marker="d",
        s=100,
        facecolor="none",
        label=LABELS[1],
    )
    ax[0].set(xlabel="real part", ylabel="imag part")
    ax[0].add_patch(Rectangle((-1, -50), 2, 100, color=colors[7], alpha=0.5))

    ax[1].scatter(
        np.real(eigs_no_stability_gau),
        np.imag(eigs_no_stability_gau),
        color=colors[0],
        linewidths=2,
        marker="o",
        s=100,
        facecolor="none",
        label=LABELS[0],
    )
    ax[1].scatter(
        np.real(eigs_stability_gau),
        np.imag(eigs_stability_gau),
        color=colors[1],
        linewidths=2,
        marker="d",
        s=100,
        facecolor="none",
        label=LABELS[1],
    )
    ax[1].set(xlabel="real part", ylabel="imag part")
    ax[1].set_facecolor(colors[7])
    ax[1].set_xticks([-0.5, 0, 1])
    ax[0].legend(loc="center", bbox_to_anchor=(0.5, 1.1))
    ax[0].legend()
    plt.xlim([-0.5, 1])
    # plt.ylim([-80, 80])
    plt.tight_layout()

    fig.savefig(Params.path + "../" + "eigenvalue_comparison.pdf")
    fig.savefig(Params.path + "../" + "eigenvalue_comparison.png", dpi=300)


def linear_model(t, x):
    return A_learn @ x + B_learn


x = np.arange(0, 1, 1 / 1000)
y_testing = np.array(data["t"].T)
x, y_testing = np.meshgrid(x, y_testing)
Err_testing = []

for k in range(X_testing.shape[0]):

    x0 = U[:, :r].T @ X_testing[k, :, 0]
    t_testing = np.arange(0, 501) * (t_true[1] - t_true[0])

    sol_OpInf = solve_ivp(
        linear_model,
        [t_testing[0], t_testing[-1]],
        x0,
        t_eval=t_testing,
        atol=1e-8,
        rtol=1e-8,
    )
    full_sol_OpInf = U[:, :r] @ sol_OpInf.y

    if (sol_OpInf.y).shape[-1] == len(t_testing):
        fig, ax = plt.subplots(1, 3, figsize=(15, 4), subplot_kw={"projection": "3d"})
        # Plot the surface.
        surf = ax[0].plot_surface(x, y_testing, X_testing[k].T, cmap=cm.coolwarm)
        surf = ax[1].plot_surface(x, y_testing, full_sol_OpInf.T, cmap=cm.coolwarm)
        surf = ax[2].plot_surface(
            x,
            y_testing,
            np.log10(np.abs(X_testing[k].T - full_sol_OpInf.T)),
            cmap=cm.coolwarm,
        )
        ax[0].set(xlabel="$x$", ylabel="$t$", zlabel="$u(x,t)$")
        ax[1].set(xlabel="$x$", ylabel="$t$", zlabel="$\hat{u}(x,t)$")
        ax[2].set(xlabel="$x$", ylabel="$t$", zlabel="Error in log-scale")

        ax[0].set_zlim([0, 2])
        ax[1].set_zlim([0, 2])

        for _ax in ax:
            _ax.xaxis.labelpad = 10
            _ax.yaxis.labelpad = 10
            _ax.zaxis.labelpad = 10

        plt.tight_layout()

        fig.savefig(Params.path + f"transient_comparison_{k}.pdf")
        fig.savefig(Params.path + f"transient_comparison_{k}.png", dpi=300)

        err = (np.linalg.norm(full_sol_OpInf - X_testing[k])) / (
            np.linalg.norm(X_testing[k])
        )
        Err_testing.append(err)

    else:
        Err_testing.append(np.NaN)


Err_testing


savemat(FILE_PATH, {"vecs": Phi, "eigs": eigs, "Err_testing": Err_testing})


if Params.plot_eigenvalue_comparison:
    err_no_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeNoStability/"
        + "dominant_eigen_vectors_no_stability_gua.mat"
    )["Err_testing"][0]
    err_stability_gau = loadmat(
        Params.path
        + "../"
        + "ImposeStability/"
        + "dominant_eigen_vectors_stability_gua.mat"
    )["Err_testing"][0]
    labels = ["test1", "test2", "test3"]
    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width / 2, err_no_stability_gau, width, label=LABELS[0])
    rects2 = ax.bar(x + width / 2, err_stability_gau, width, label=LABELS[1])

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel("relative error")
    # ax.set_title('Scores by group and gender')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    plt.tight_layout()

    fig.savefig(Params.path + "../" + "test_comparison.pdf")
    fig.savefig(Params.path + "../" + "test_comparison.png", dpi=300)
