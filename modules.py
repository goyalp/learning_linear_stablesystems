import sys
import torch
import torch.nn as nn

import utils


class ModelHypothesisStable(nn.Module):

    """
    Here we make model hypothesis for stable models.
    """

    def __init__(self, sys_order, B_term=True):

        """
        Define model parameters.
            A = (J-R)Q,
        where J is skew-symmetric, R and Q are positive semi-definite matrics.
        This then guarantees stability of A.
        """
        print("Stability Gurantees!")

        super().__init__()
        fac = 10
        self.B_term = B_term
        self._J = torch.nn.Parameter(torch.randn(sys_order, sys_order) / fac)
        self._R = torch.nn.Parameter(torch.randn(sys_order, sys_order) / fac)
        self._Q = torch.nn.Parameter(torch.randn(sys_order, sys_order) / fac)

        J = self._J - self._J.T
        R = self._R @ self._R.T

        Q = self._Q @ self._Q.T
        A = (J - R) @ Q

        self.A = A

        self.B = torch.nn.Parameter(
            torch.zeros(
                sys_order,
            )
            / fac
        )

    def forward(self, x):
        J = self._J - self._J.T
        R = self._R @ self._R.T

        Q = self._Q @ self._Q.T
        A = (J - R) @ Q

        self.A = A
        if self.B_term:
            return x @ A.T + self.B
        else:
            return x @ A.T


class ModelHypothesis(nn.Module):
    """
    Here we make model hypothesis for stable models.
    """

    def __init__(self, sys_order, B_term=True):
        print("No Stability Gurantees!")
        super().__init__()
        self.B_term = B_term
        fac = 10
        self.A = torch.nn.Parameter(torch.randn(sys_order, sys_order) / fac)

        self.B = torch.nn.Parameter(
            torch.zeros(
                sys_order,
            )
            / fac
        )

    def forward(self, x):

        if self.B_term:
            return x @ self.A.T + self.B
        else:
            return x @ self.A.T


def training(models, dataloaders, opt_func, Params, scheduler=None):

    """
    This is a training function.
    Inputs:
       models: defines vector field.
       dataloaders: contains training data
       opt_func: optimizer
       Params: contains additional parametes such as epochs
       schedulers: learning rate scheduler


    Returns:
        trained 'models', loss_track, learning_rate_track
    """

    criteria = nn.MSELoss()
    loss_track = []
    learning_rate_track = []

    for g in range(Params.num_epochs):

        for y in dataloaders["train"]:
            y = y[0]

            opt_func.zero_grad()
            total_loss = 0.0

            encoding_pred = utils.rk4th_onestep(
                models["modelDyn"], y[:, :-1, :], timestep=Params.time_step
            )

            total_loss += (1 / Params.time_step) * criteria(encoding_pred, y[:, 1:, :])

            total_loss = total_loss / y[0].shape[0]
            loss_track.append(total_loss.item())
            total_loss.backward()
            opt_func.step()
            if scheduler:
                scheduler.step()
            learning_rate_track.append(opt_func.param_groups[0]["lr"])

        if g and (g + 1) % 100 == 0:

            lr = opt_func.param_groups[0]["lr"]
            sys.stdout.write(
                f"\r [{g + 1} /{Params.num_epochs}] [Training loss: {loss_track[g]:.2e}] [Learning rate: {lr:.2e}]"
            )

    return models, loss_track, learning_rate_track
