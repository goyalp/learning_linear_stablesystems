#!/bin/bash

python --version 

EPOCHS=12000

DIR="./Results"


echo "##################################################################"
echo "############# Running Cylider wake example ##############"
echo "##################################################################"

python Example_Fluid.py --epochs $EPOCHS
echo ""
python Example_Fluid.py --epochs $EPOCHS --imposestability --plot_eigenvalue_comparison
echo ""

echo "##################################################################"
echo "############# Running Superposed flow ##############"
echo "##################################################################"

python Example_Superposedflow.py --epochs $EPOCHS
echo ""
python Example_Superposedflow.py --epochs $EPOCHS --imposestability --plot_eigenvalue_comparison
echo ""

echo "##################################################################"
echo "############# Running Burgers example #####"
echo "##################################################################"

python Example_Burgers.py --epochs $EPOCHS
echo ""
python Example_Burgers.py --epochs $EPOCHS --imposestability --plot_eigenvalue_comparison
echo ""
