" "
import os
import random

import torch
import numpy as np


def rk4th_onestep(model, x, timestep=1e-2):
    """
    It defines 4th-order Runge-Kutta step to predict x at next time-step.

    Args:
        model: it defines vector field
        x: x at time t
        timestep (optional): time-step. Defaults to 1e-2.

    Returns:
        x at time t + timestep
    """
    k1 = model(x)
    k2 = model(x + 0.5 * timestep * k1)
    k3 = model(x + 0.5 * timestep * k2)
    k4 = model(x + 1.0 * timestep * k3)
    return x + (1 / 6) * (k1 + 2 * k2 + 2 * k3 + k4) * timestep


def reproducibility_seed(seed: int) -> None:
    """To set seed for random variables."""
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = False
